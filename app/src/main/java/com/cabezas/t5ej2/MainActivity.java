package com.cabezas.t5ej2;

import android.app.Activity;
import android.app.Service;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toolbar;

import java.util.ArrayList;


public class MainActivity extends Activity {

    private ArrayList<Tarea> tareas;
    private TareaRecyclerAdapter mAdapter;
    private EditText tareaEdit;
    private ImageButton button;
    private boolean add = false;
    private InputMethodManager teclado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        teclado = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setActionBar(toolbar);

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            tareas = new ArrayList<Tarea>();
            mAdapter = new TareaRecyclerAdapter(this, tareas, R.layout.item);
            recyclerView.setAdapter(mAdapter);
            tareaEdit = (EditText) findViewById(R.id.tareaEditText);
            button = (ImageButton) findViewById(R.id.add_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!add) {
                        tareaEdit.setVisibility(View.VISIBLE);
                        tareaEdit.requestFocus();
                        //Mostrar teclado
                        teclado.showSoftInput(tareaEdit, 0);
                        button.setImageDrawable(getDrawable(android.R.drawable.ic_menu_save));
                    } else {
                        button.setImageDrawable(getDrawable(android.R.drawable.ic_input_add));
                        tareas.add(new Tarea(tareaEdit.getText().toString()));
                        mAdapter.notifyDataSetChanged();
                        teclado.hideSoftInputFromWindow(tareaEdit.getWindowToken(), 0);
                        tareaEdit.setText("");
                        tareaEdit.setVisibility(View.GONE);
                    }
                    add = !add;
                }
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
