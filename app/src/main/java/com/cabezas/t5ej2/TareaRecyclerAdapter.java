package com.cabezas.t5ej2;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by manel on 19/09/2015.
 */
public class TareaRecyclerAdapter extends RecyclerView.Adapter<TareaRecyclerAdapter.ViewHolder> {
    private List<Tarea> items;
    private int itemLayout;
    private Context context;

    public TareaRecyclerAdapter(Context context, List<Tarea> items, int itemLayout) {
        this.context = context;
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Tarea item = items.get(position);

        holder.title.setText(item.getTexto());


    }


    public void add(Tarea item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Tarea item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView title;

        public ViewHolder(View itemView) {
            super(itemView);


            title = (TextView) itemView.findViewById(R.id.texto);


        }
    }
}
