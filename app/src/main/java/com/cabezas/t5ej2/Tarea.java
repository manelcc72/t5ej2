package com.cabezas.t5ej2;

/**
 * Created by manel on 19/09/2015.
 */
public class Tarea {

    String texto;

    public Tarea(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
